# IP Blocker Middleware

`IpBlocker::Middleware` provides a simple manner of blocking specific
IP's from ever reaching your Rack/Rails application. It does this by
reading from a list of IP's specified in a plain text file.

## Installation

Put this in your Gemfile:

```ruby
gem "rack-ip_blocker", require: "ip_blocker"
```

Create your blacklist file:

```sh
touch config/blocked_ips.txt
```

## Usage and configuration

### Rails

When using IpBlocker with Rails, the middleware is automatically loaded.
By default, IpBlocker will try to read blocked IP's from `config/blocked_ips.txt`.

To specify the location of your IP blacklist file, put this in your
enviroment files (`config/environments/production.rb`, etc):

```ruby
YourApp::Application.configure do

  # ...

  config.ip_blocker.file_path = Rails.root.join("config/my_blacklist.txt").to_s

  # ...

end
```

### Other Rack applications

The middleware can be manually included like so:

```ruby
use IpBlocker::Middleware, file_path: "/path/to/my/blacklist.txt"
```

## License

See LICENSE.
