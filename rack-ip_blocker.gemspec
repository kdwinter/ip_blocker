$:.push File.expand_path("../lib", __FILE__)
require 'ip_blocker/version'

Gem::Specification.new do |s|
  s.name        = "rack-ip_blocker"
  s.version     = IpBlocker::VERSION
  s.authors     = ["Kenneth De Winter"]
  s.email       = ["kdwinter@protonmail.com"]
  s.homepage    = "https://gitlab.com/gigamo/ip_blocker"
  s.summary     = "Simple Rack middleware that blocks manually blacklisted IP's."
  s.description = s.summary

  s.files         = `git ls-files`.split($/).reject { |f| f == '.gitignore' or f =~ /^examples/ }
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ["lib"]

  s.add_development_dependency "bundler", "~> 1.3"
  s.add_development_dependency "rake"
  s.add_development_dependency "minitest", ">= 5.3.0"
  s.add_development_dependency "rack-test", ">= 0"
end
