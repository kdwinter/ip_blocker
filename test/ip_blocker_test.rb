require "ip_blocker"
require "minitest/autorun"
require "rack/test"

describe IpBlocker::Middleware do
  include Rack::Test::Methods

  def load_app(file_path)
    Rack::Builder.new do
      use IpBlocker::Middleware, file_path: File.join(Dir.pwd, file_path)
      map("/") do
        run ->(env) { [200, {"Content-Type" => "text/plain"}, ["OK"]] }
      end
    end
  end

  def teardown
    # Don't cache
    IpBlocker::Middleware.class_variable_set(:@@blocked_ips, nil)
  end

  describe "with IP not in blacklist" do
    let(:app) { load_app("test/blacklist_empty.txt") }

    it "should let requests through" do
      get "/"
      assert_equal 200, last_response.status
      assert_equal "OK", last_response.body
    end
  end

  describe "with IP in blacklist" do
    let(:app) { load_app("test/blacklist_localhost.txt") }

    it "should block requests" do
      get "/"
      assert_equal 403, last_response.status
      assert_equal "<h1>403 Forbidden</h1>", last_response.body
    end
  end
end
