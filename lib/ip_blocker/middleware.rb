module IpBlocker
  class Middleware
    # List of blocked IPs. Use Set instead of Array for faster
    # +include?+ lookups, as order or uniqueness doesn't matter here.
    @@blocked_ips = nil
    def self.blocked_ips(file_path)
      @@blocked_ips ||= Set.new(
        # Remove newlines at the end of each line.
        File.readlines(file_path).map { |line|
          line.chomp.strip
        }.reject { |line|
          # Skip empty lines or comments.
          line.empty? || line.start_with?("#")
        }.freeze.map(&:freeze)
      )
    rescue Errno::ENOENT
      warn "IpBlocker: File with blocked IP's '#{file_path}' doesn't exist. " \
        "Not blocking anything."
      @@blocked_ips = []
    end

    # Response to return when the request IP is blocked.
    BLOCKED_RESPONSE = [
      403,
      {"Content-Type".freeze => "text/html".freeze},
      ["<h1>403 Forbidden</h1>".freeze]
    ]
    RACK_IP_ENV_KEY  = "REMOTE_ADDR".freeze
    RAILS_IP_ENV_KEY = "action_dispatch.remote_ip".freeze

    def initialize(app, options = {})
      @app = app
      @options = options

      if rails?
        @options[:file_path] ||= Rails.configuration.ip_blocker.file_path
      end
    end

    def call(env)
      ip = env.fetch(
        rails? ? RAILS_IP_ENV_KEY : RACK_IP_ENV_KEY,
        "".freeze
      ).to_s

      # IP is blocked, return the blocked response.
      if blocked_ips.include?(ip)
        BLOCKED_RESPONSE
      # Let the request through.
      else
        @app.call(env)
      end
    end

  protected

    def blocked_ips
      self.class.blocked_ips(@options[:file_path])
    end

    def rails?
      defined?(Rails)
    end
  end
end
