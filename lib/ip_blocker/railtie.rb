module IpBlocker
  class Railtie < Rails::Railtie
    config.ip_blocker = ActiveSupport::OrderedOptions.new

    initializer :"ip_blocker.set_default_file_path" do
      config.ip_blocker.file_path ||= Rails.root.join("config/blocked_ips.txt").to_s
    end

    initializer :"ip_blocker.insert_middleware" do |app|
      app.middleware.insert_after ActionDispatch::RemoteIp,
        IpBlocker::Middleware
    end
  end
end
